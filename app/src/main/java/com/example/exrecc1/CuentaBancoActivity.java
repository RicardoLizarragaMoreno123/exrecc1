package com.example.exrecc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class CuentaBancoActivity extends AppCompatActivity {

    private EditText numeroCuenta;
    private EditText nombreCliente;
    private EditText banco;
    private EditText saldo;
    private RadioGroup movimientos;
    private EditText nuevoSaldo;
    private Button btnRegistrarCuenta;
    private Button btnAplicarMovimiento;
    private Button btnConsultarSaldo;
    private TextView tvSaldoActual;
    private double saldoActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);

        numeroCuenta = findViewById(R.id.numeroCuenta);
        nombreCliente = findViewById(R.id.nombreCliente);
        banco = findViewById(R.id.banco);
        saldo = findViewById(R.id.saldo);
        movimientos = findViewById(R.id.movimientos);
        nuevoSaldo = findViewById(R.id.nuevoSaldo);
        btnRegistrarCuenta = findViewById(R.id.btnRegistrarCuenta);
        btnAplicarMovimiento = findViewById(R.id.btnAplicarMovimiento);
        btnConsultarSaldo = findViewById(R.id.btnConsultarSaldo);
        tvSaldoActual = findViewById(R.id.tvSaldoActual);

        btnRegistrarCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numeroCuenta.getText().toString().isEmpty() || nombreCliente.getText().toString().isEmpty()
                        || banco.getText().toString().isEmpty() || saldo.getText().toString().isEmpty()) {
                    Toast.makeText(CuentaBancoActivity.this, "Por favor, complete todos los campos", Toast.LENGTH_SHORT).show();
                } else {
                    saldoActual = Double.parseDouble(saldo.getText().toString());
                    tvSaldoActual.setText("Saldo Actual: $" + saldoActual);
                    Toast.makeText(CuentaBancoActivity.this, "Cuenta registrada exitosamente", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnAplicarMovimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (movimientos.getCheckedRadioButtonId() == -1 || nuevoSaldo.getText().toString().isEmpty()) {
                    Toast.makeText(CuentaBancoActivity.this, "Seleccione un movimiento y capture la cantidad", Toast.LENGTH_SHORT).show();
                } else {
                    double cantidad = Double.parseDouble(nuevoSaldo.getText().toString());
                    if (movimientos.getCheckedRadioButtonId() == R.id.rbDeposito) {
                        saldoActual += cantidad;
                        tvSaldoActual.setText("Saldo Actual: $" + saldoActual);
                        Toast.makeText(CuentaBancoActivity.this, "Depósito realizado. Saldo actual: " + saldoActual, Toast.LENGTH_SHORT).show();
                    } else if (movimientos.getCheckedRadioButtonId() == R.id.rbRetiro) {
                        if (cantidad <= saldoActual) {
                            saldoActual -= cantidad;
                            tvSaldoActual.setText("Saldo Actual: $" + saldoActual);
                            Toast.makeText(CuentaBancoActivity.this, "Retiro realizado. Saldo actual: " + saldoActual, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CuentaBancoActivity.this, "Saldo insuficiente", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        btnConsultarSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CuentaBancoActivity.this, "Saldo actual: " + saldoActual, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
